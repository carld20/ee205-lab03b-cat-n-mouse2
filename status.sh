#!/bin/bash

echo "I am"
echo $(echo $(whoami))
echo "The current working directory is"
echo $(echo $(pwd))
echo "The system I am on is"
echo $(echo $(hostname))
echo "The Linux version is"
echo $(echo $(uname -r))
echo "The Linux distribution is"
echo $(echo $(cat /etc/system-release))
echo "The system has been up for"
echo $(echo $(uptime))
echo "The amount of disk space I'm using in KB is"
echo $(echo $( du -k ~ | tail -1))

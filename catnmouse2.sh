#!/bin/bash

DEFAULT_MAX_NUMBER=2048

THE_MAX_VALUE=${1:-$DEFAULT_MAX_NUMBER}

echo The max value is $THE_MAX_VALUE

THE_NUMBER_IM_THINKING_OF=$((RANDOM%$THE_MAX_VALUE+1))

#echo The number I\'m thinking of is $THE_NUMBER_IM_THINKING_OF
if [[$THE_MAX_VALUE -ge 1]]
then
	while [$user_guess -ne $THE_NUMBER_IM_THINKING_OF]
       	do
		read user_guess
		echo "OK cat, I'm thinking of a number from 1 to $THE_MAX_VALUE"
		if [$user_guess -eq $THE_NUMBER_IM_THINKING_OF]
		then 
			echo "You got me"
			exit 0
		elif [$user_guess -lt 1]
		then
			echo "You must enter a number that's >= 1"
		elif [$user_guess -gt $THE_MAX_VALUE]
		then
			echo "You must enter a number that's <= $THE_MAX_VALUE"
		elif [$user_guess -gt $THE_NUMBER_IM_THINKING_OF]
		then
			echo "No cat... the number I'm thinking of is smaller than $user_guess"
		elif [$user_guess -lt $THE_NUMBER_IM_THINKING_OF]
		then
			echo "No cat.. the number I'm thinking of is larger than $user_guess"
		fi
	done
else
	echo "ERROR NUMEBR MUST >= 1"
	exit 1
fi
